import task1_2.sortings.implementations.BubbleSorting;
import task1_2.sortings.implementations.InsertionSorting;
import task1_2.sortings.implementations.SelectionSorting;
import task3.StudentsSorter;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Integer[] numbers = {1, 35, 64, 24, 2, 0};
        Character[] characters = {'a', 'g', 'c', 'b', 'h', 'z', 'w'};
        String[] strings = {"bac", "a", "b", "ab", "ba", "abc"};

        // task 1 tests

//        var insertionSorting = new InsertionSorting<Integer>();
//        var bubbleSorting = new BubbleSorting<Integer>();
//        var selectionSorting = new SelectionSorting<Integer>();
//
//        insertionSorting.sort(numbers);
//        System.out.println(Arrays.toString(numbers));
//
//        insertionSorting.sortDescending(numbers);
//        System.out.println(Arrays.toString(numbers));

//        bubbleSorting.sort(numbers);
//        System.out.println(Arrays.toString(numbers));
//
//        bubbleSorting.sortDescending(numbers);
//        System.out.println(Arrays.toString(numbers));
//
//        selectionSorting.sort(numbers);
//        System.out.println(Arrays.toString(numbers));
//
//        selectionSorting.sortDescending(numbers);
//        System.out.println(Arrays.toString(numbers));



//        var insertionSorting = new InsertionSorting<Character>();
//        var bubbleSorting = new BubbleSorting<Character>();
//        var selectionSorting = new SelectionSorting<Character>();

//        insertionSorting.sort(characters);
//        System.out.println(Arrays.toString(characters));
//
//        insertionSorting.sortDescending(characters);
//        System.out.println(Arrays.toString(characters));
//
//        bubbleSorting.sort(characters);
//        System.out.println(Arrays.toString(characters));
//
//        bubbleSorting.sortDescending(characters);
//        System.out.println(Arrays.toString(characters));
//
//        selectionSorting.sort(characters);
//        System.out.println(Arrays.toString(characters));
//
//        selectionSorting.sortDescending(characters);
//        System.out.println(Arrays.toString(characters));


        //task 2 tests

//        var insertionSorting = new InsertionSorting<String>();
//        var bubbleSorting = new BubbleSorting<String>();
//        var selectionSorting = new SelectionSorting<String>();
//
//        insertionSorting.sort(strings);
//        System.out.println(Arrays.toString(strings));
//
//        insertionSorting.sortDescending(strings);
//        System.out.println(Arrays.toString(strings));

//        bubbleSorting.sort(strings);
//        System.out.println(Arrays.toString(strings));
//
//        bubbleSorting.sortDescending(strings);
//        System.out.println(Arrays.toString(strings));

//        selectionSorting.sort(strings);
//        System.out.println(Arrays.toString(strings));
//
//        selectionSorting.sortDescending(strings);
//        System.out.println(Arrays.toString(strings));


        // task 3 tests

//        String[] names = {"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
//        String[] surnames = {"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};
//
//        StudentsSorter.sort(names, surnames);
//
//        System.out.println(Arrays.toString(names));
//        System.out.println(Arrays.toString(surnames));
    }
}