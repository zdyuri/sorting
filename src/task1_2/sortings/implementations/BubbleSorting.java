package task1_2.sortings.implementations;

import task1_2.sortings.abstractions.SortingBase;

import java.util.function.BiPredicate;

/**
 * Provides functionality for sorting arrays using bubble sorting algorithm.
 * @param <T> Type of array elements.
 */
public class BubbleSorting<T extends Comparable<T>> extends SortingBase<T> {

    /**
     * Sorts array using bubble sort algorithm.
     * @param items Array to sort.
     * @param sortingPredicate Predicate for sorting of array items in a proper order.
     */
    @Override
    protected void sortCore(T[] items, BiPredicate<T, T> sortingPredicate) {
        for (var i = items.length; i > 0; i--) {
            boolean elementsWereSwapped = false;

            for (var j = 0; j < i - 1; j++) {
                if (sortingPredicate.test(items[j], items[j + 1])) {
                    swap(items, j, j + 1);
                    elementsWereSwapped = true;
                }
            }

            if (!elementsWereSwapped) {
                return;
            }
        }
    }
}
