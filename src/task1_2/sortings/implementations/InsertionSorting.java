package task1_2.sortings.implementations;

import task1_2.sortings.abstractions.SortingBase;

import java.util.function.BiPredicate;

/**
 * Provides functionality for sorting arrays using insertion sorting algorithm.
 * @param <T> Type of array elements.
 */
public class InsertionSorting <T extends Comparable<? super T>> extends SortingBase<T> {

    /**
     * Sorts array using insertion sort algorithm.
     * @param items Array to sort.
     * @param sortingPredicate Predicate for sorting of array items in a proper order.
     */
    @Override
    protected void sortCore(T[] items, BiPredicate<T, T> sortingPredicate) {
        for (var i = 1; i < items.length; i++) {
            T currentValue = items[i];

            var insertionIndex = findInsertionPosition(items, i, items[i], sortingPredicate);

            System.arraycopy(items, insertionIndex, items, insertionIndex + 1, i - insertionIndex);

            items[insertionIndex] = currentValue;
        }
    }

    private int findInsertionPosition(T[] items, int sliceLength, T value, BiPredicate<T, T> sortingPredicate) {
        for (var i = 0; i < sliceLength; i++) {
            if (sortingPredicate.test(items[i], value)) {
                return i;
            }
        }

        return sliceLength;
    }
}
