package task1_2.sortings.implementations;

import task1_2.sortings.abstractions.SortingBase;

import java.util.function.BiPredicate;

/**
 * Provides functionality for sorting arrays using selection sorting algorithm.
 * @param <T> Type of array elements.
 */
public class SelectionSorting<T extends Comparable<T>> extends SortingBase<T> {

    /**
     * Sorts array using selection sort algorithm.
     * @param items Array to sort.
     * @param sortingPredicate Predicate for sorting of array items in a proper order.
     */
    @Override
    protected void sortCore(T[] items, BiPredicate<T, T> sortingPredicate) {
        for (var i = 0; i < items.length - 1; i++) {
            int targetArrayItemIndex = i;

            for (var j = i + 1; j < items.length; j++) {
                if (sortingPredicate.test(items[targetArrayItemIndex], items[j])) {
                    targetArrayItemIndex = j;
                }
            }

            swap(items, targetArrayItemIndex, i);
        }
    }
}
