package task1_2.sortings.abstractions;

import java.util.function.BiPredicate;

/**
 * Base abstract class for all sorting algorithms.
 * @param <T> Type of array elements.
 */
public abstract class SortingBase<T extends Comparable<? super T>> implements ISorting<T> {

    /**
     {@inheritDoc}
     */
    public void sort(final T[] items) {
        this.sortInternal(items, this::ascendingOrderPredicate);
    }

    /**
     {@inheritDoc}
     */
    public void sortDescending(final T[] items) {
        this.sortInternal(items, this::descendingOrderPredicate);
    }


    /**
     * Safe sort of array.
     * @param items Array to sort.
     * @param sortingPredicate Predicate for sorting of array items in a proper order.
     */
    protected void sortInternal(T[] items, BiPredicate<T, T> sortingPredicate) {
        assertArray(items);

        if (items.length < 2) {
            return;
        }

        sortCore(items, sortingPredicate);
    }

    /**
     * Sorts array using specific sorting algorithm.
     * @param items Array to sort.
     * @param sortingPredicate Predicate for sorting of array items in a proper order.
     */
    protected abstract void sortCore(T[] items, BiPredicate<T, T> sortingPredicate);

    /**
     * Swaps two elements in array.
     * @param items Source array.
     * @param firstElementIndex Index of first swapped element.
     * @param secondElementIndex Index of second swapped element.
     */
    protected static <T> void swap(T[] items, int firstElementIndex, int secondElementIndex) {
        T temp = items[firstElementIndex];
        items[firstElementIndex] = items[secondElementIndex];
        items[secondElementIndex] = temp;
    }

    // Checks array for validity.
    // Throws IllegalArgumentException if source array is null.
    private static <T> void assertArray(T[] items) throws IllegalArgumentException {
        if (items == null) {
            throw new IllegalArgumentException("Source array is null.");
        }
    }

    // Predicate for sorting an array in ascending order.
    private boolean ascendingOrderPredicate(T item1, T item2) {
        return item2.compareTo(item1) < 0;
    }

    // Predicate for sorting an array in descending order.
    private boolean descendingOrderPredicate(T item1, T item2) {
        return item2.compareTo(item1) >= 0;
    }
}