package task1_2.sortings.abstractions;

/**
 * Base interface for all sorting algorithms.
 * @param <T> Type of array elements.
 */
public interface ISorting<T extends Comparable<? super T>> {
    /**
     * Sorts array in ascending order.
     * @param items Array to sort in ascending order.
     */
    void sort(T[] items);

    /**
     * Sorts array in descending order.
     * @param items Array to sort in descending order.
     */
    void sortDescending(T[] items);
}
