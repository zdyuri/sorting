package task3;

public class StudentsSorter {
    public static void sort(String[] names, String[] surnames) {
        assertArrays(names, surnames);

        for (var i = 0; i < names.length; i++){
            int targetArrayItemIndex = i;

            for (var j = i + 1; j < names.length; j++) {
                var compareResult = names[j].compareTo(names[targetArrayItemIndex]);

                if (compareResult == 0 && surnames[j].compareTo(surnames[targetArrayItemIndex]) < 0) {
                    targetArrayItemIndex = j;
                } else if (compareResult < 0) {
                    targetArrayItemIndex = j;
                }
            }

            swapStudents(names, surnames, targetArrayItemIndex, i);
        }
    }

    private static void assertArrays(String[] names, String[] surnames) {
        if (names == null || surnames == null) {
            throw new IllegalArgumentException("One or both of arrays are null.");
        }

        if (names.length != surnames.length) {
            throw new IllegalArgumentException("Arrays are not of equal sizes.");
        }
    }

    private static void swapStudents(String[] names, String[] surnames, int firstIndex, int secondIndex) {
        swap(names, firstIndex, secondIndex);
        swap(surnames, firstIndex, secondIndex);
    }

    private static void swap(String[] array, int firstIndex, int secondIndex) {
        var temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}
